module.exports = {
  siteMetadata: {
    title: `Website Title`,
    tagline: ``,
    description: ``,
    author: `@matrixcreate`,
    address: {
      line1: `123 Street Address`,
      line2: `Town`,
      line3: `County`,
      line4: `Country`,
      line5: `Postcode`
    },
    email: `info@website.co.uk`,
    phone: `01234 567890`,
    siteUrl: `https://website.netlify.app`,
    ogImage: `https://website.netlify.app/`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/assets/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
