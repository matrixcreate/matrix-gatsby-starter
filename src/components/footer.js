import React from "react"
import { Link } from "gatsby"
import PropTypes from "prop-types"
import logo from "../assets/images/brand@2x.png"

const Footer = ({ props, siteTitle, siteAddress, siteEmail, sitePhone }) => (
    <footer className="footer">
        <div className="container footer__wrapper">
            <div className="footer__column">
                <img className="brand" src={logo} alt="Logo" />
                <p>&copy; 2020 { siteTitle }, All Rights Reserved</p>
            </div>
            <div className="footer__column">
                <h3>Contact</h3>
                <p>{ siteAddress.line1 }, { siteAddress.line2 }, { siteAddress.line3 }, { siteAddress.line4 }, { siteAddress.line5 }</p>
                <p><a href={`mailto:${sitePhone}`}>{ sitePhone }</a></p>
                <p><a href={`mailto:${siteEmail}`}>{ siteEmail }</a></p>
                <ul className="footer__nav">
                    <li>
                        <Link 
                            to="/"
                            activeClassName="active">
                            Home
                        </Link>
                    </li>
                    <li>
                        <Link 
                            to="/about/"
                            activeClassName="active">
                            About
                        </Link>
                    </li>
                    <li>
                        <Link 
                            to="/contact/"
                            activeClassName="active">
                            Contact
                        </Link>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
)

Footer.propTypes = {
  siteTitle: PropTypes.string,
  siteAddress: PropTypes.object,
  siteEmail: PropTypes.string,
  sitePhone: PropTypes.string,
}

Footer.defaultProps = {
  siteTitle: ``,
  siteAddress: ``,
  siteEmail: ``,
  sitePhone: ``,
}

export default Footer