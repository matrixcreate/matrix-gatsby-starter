import React from 'react';
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api';

const containerStyle = {
  width: '100%',
  height: '500px'
};

let center = {
  lat: 50.6996293,
  lng: -1.2928676
};

let position = {
  lat: 50.6996293,
  lng: -1.2928676
}

const onLoad = marker => {
  console.log('marker: ', marker)
}

class LocationMap extends React.Component {
    
    // Fires before component render
    componentWillMount() {
        center = {
            lat: this.props.centerLat || center.lat,
            lng: this.props.centerLng || center.lng
        }
        position = {
            lat: this.props.positionLat || position.lat,
            lng: this.props.positionLng || position.lng,
        }     
    };
    
    render() {
        return (
            <div className="location-map">
                <LoadScript
                    googleMapsApiKey="AIzaSyCwVpjlrl1MiuEp_nZeQLPP8aUyhs_BZzo"
                >
                    <GoogleMap
                        mapContainerStyle={containerStyle}
                        center={center}
                        zoom={this.props.zoom}
                    >
                        <Marker
                            onLoad={onLoad}
                            position={position}
                        />
                    </GoogleMap>
                </LoadScript>
            </div>
        )
    }
}

export default LocationMap