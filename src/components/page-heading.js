import React from 'react'

const PageHeading = ( props ) => (
    <div className="container">
        <h1 className="page-heading">{ props.text } </h1>
    </div>
)

export default PageHeading