import React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import logo from "../assets/images/brand@2x.png"
import NavBar from './navbar'

const Header = ({ props, siteTitle, siteSubtitle, siteEmail, sitePhone }) => (
    <header className="header">
        <section className="nav-wrapper">
            <div className="grid container">
                <Link
                    to="/"
                    className="brand">
                    <img className="brand" src={logo} alt="Logo" />
                </Link>
                <NavBar />
            </div>
        </section>
    </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
  siteEmail: PropTypes.string,
  sitePhone: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
  siteEmail: ``,
  sitePhone: ``,
}

export default Header