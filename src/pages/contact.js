import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
import PageHeading from "../components/page-heading"
import LocationMap from "../components/location-map"
import "../assets/sass/style.scss"

const Contact = ({ props }) => {
    
    const metadata = useStaticQuery(graphql`
        query metadataQuery {
            site {
                siteMetadata {
                    title
                    email
                    phone
                    address {
                        line1
                        line2
                        line3
                        line4
                        line5
                    }
                }
            }
        }
    `)
    
    return (
        <Layout>
            <SEO title="Location" />
            
            <PageHeading text="Contact"></PageHeading>
                
            <div className="container">
                <div className="text">
                    <p>
                        { metadata.site.siteMetadata.address.line1 }
                        <br />
                        { metadata.site.siteMetadata.address.line2 }
                        <br />
                        { metadata.site.siteMetadata.address.line3 }
                        <br />
                        { metadata.site.siteMetadata.address.line4 }
                        <br />
                        { metadata.site.siteMetadata.address.line5 }
                        <br />
                        <br />
                        <a href={`mailto:${metadata.site.siteMetadata.phone}`}>{ metadata.site.siteMetadata.phone }</a>
                        <br />
                        <a href={`mailto:${metadata.site.siteMetadata.email}`}>{ metadata.site.siteMetadata.email }</a>
                        
                    </p>
                </div>
            </div>
            
            <LocationMap
                positionLat={50.688985}
                positionLng={-1.08902}
                centerLat={50.688985}
                centerLng={-1.08902}
                zoom={15}
            ></LocationMap>

        </Layout>
    )
}

export default Contact