import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import PageHeading from "../components/page-heading"
import { Link } from "gatsby"
import "../assets/sass/style.scss"

const NotFoundPage = () => {    
    
    return (
        <Layout>
            <SEO title="Location" />
            
            <PageHeading text="404 – Page Not Found"></PageHeading>
                
            <div className="container">
                <div className="text">
                    <p>
                        Sorry, we can't locate that page. Please try the&nbsp;
                        <Link
                            to="/"
                            className="brand">
                             homepage
                        </Link>
                      .
                    </p>
                </div>
            </div>

        </Layout>
    )
}

export default NotFoundPage