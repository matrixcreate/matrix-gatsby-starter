import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import PageHeading from "../components/page-heading"
import "../assets/sass/style.scss"

const About = ({ props }) => {    
    
    return (
        <Layout>
            <SEO title="Location" />
            
            <PageHeading text="About"></PageHeading>
                
            <div className="container">
                <div className="text">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto officiis natus dignissimos cupiditate asperiores? Necessitatibus porro exercitationem praesentium, laborum asperiores tempora doloremque voluptates temporibus doloribus aliquid corrupti tempore, quis incidunt.</p>
                </div>
            </div>

        </Layout>
    )
}

export default About