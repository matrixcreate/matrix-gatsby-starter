import React from "react"
import Layout from "../components/layout"
import Hero from "../components/hero"
import SEO from "../components/seo"
import "../assets/sass/style.scss"

const IndexPage = (props) => (

    <Layout>
        <SEO title="Home" />
        
        <Hero>
            <div className="hero container">
                <div className="hero__content">
                    Hero text
                </div>
            </div>
        </Hero>
        
        <div className="">
            <div className="grid grid--flex">
                <div className="column column--image">
                    image
                </div>
                <div className="column column--text">
                    text
                </div>
            </div>
            
            <div className="grid grid--flex grid--reverse">
                <div className="column column--image">
                    image
                </div>
                <div className="column column--text">
                    text
                </div>
            </div>
        </div>
    </Layout>
)

export default IndexPage

export const fluidImage = graphql`
    fragment homeImages on File {
      childImageSharp {
        fluid(maxWidth: 1000) {
          ...GatsbyImageSharpFluid_withWebp_tracedSVG
        }
      }
    }
`;

export const pageQuery = graphql`
  query {
    homeImages: file(relativePath: { eq: "hero_bg.jpg" }) {
      ...homeImages
    }
    jonathanImage: file(relativePath: { eq: "jonathan_photo@2x.png" }) {
      ...homeImages
    }
    keyVisual_01: file(relativePath: { eq: "key_visual_01_left@2x.jpg" }) {
      ...homeImages
    }
    keyVisual_02: file(relativePath: { eq: "key_visual_02_right@2x.png" }) {
      ...homeImages
    }
  }
`