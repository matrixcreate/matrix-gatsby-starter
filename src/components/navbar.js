import React, { useState } from 'react';
import { Link } from "gatsby"
import { slide as Menu } from 'react-burger-menu'
import styled from '@emotion/styled'
import navIcon from "../assets/images/nav_icon.svg"
import navClose from "../assets/images/nav_close.svg"

export default () => {

  const [menuState, setMenuOpen] = useState({menuOpen: false});

  const closeMenu = () => {
    document.querySelector('html').classList.remove('no-scroll');
    setMenuOpen({menuOpen: false})
  }

  const handleOnOpen = () => {
    document.querySelector('html').classList.add('no-scroll');
    setMenuOpen({menuOpen: true})
  }
  const handleOnClose = () => {
    document.querySelector('html').classList.remove('no-scroll');
    setMenuOpen({menuOpen: false})
  }

  return (
    <>
        <TopNav className="navbar">
            <NavItems>
                <Link 
                    to="/"
                    activeClassName="active">
                    Home
                </Link>
                <Link 
                    to="/about/"
                    activeClassName="active">
                    About
                </Link>
                <Link 
                    to="/contact/"
                    activeClassName="active">
                    Contact
                </Link>
                <Link 
                    to="/"
                    className="button"
                    activeClassName="active">
                    Action
                </Link>
            </NavItems>

            
            <SideMenu>
                <Menu isOpen={ menuState.menuOpen }
                    onOpen={ handleOnOpen }
                    onClose={ handleOnClose }
                    width={ '100%' }
                    customBurgerIcon={ <img src={navIcon} alt="Menu" /> }
                    customCrossIcon={ <img src={navClose} alt="Close menu" /> }
                    right>
                    <Link 
                        to="/"
                        className="menu-item"
                        activeClassName="active"
                        onClick={() => closeMenu() }>
                        Home
                    </Link>
                    <Link 
                        to="/about/"
                        className="menu-item"
                        activeClassName="active"
                        onClick={() => closeMenu() }>
                        About
                    </Link>
                    <Link 
                        to="/contact/"
                        className="menu-item"
                        activeClassName="active"
                        onClick={() => closeMenu() }>
                        Contact
                    </Link>
                    <Link 
                        to="/Action/"
                        className="menu-item"
                        activeClassName="active"
                        onClick={() => closeMenu() }>
                        Action
                    </Link>
                  </Menu>
            </SideMenu>
        </TopNav>
    </>
)}

const TopNav = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
`

const NavItems = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-end;
  align-items: center;
  a {
    display: flex;
    align-items: center;
    text-align: center;
    margin-left: 1em;
    padding: 0.25em 1em;
    font-size: 0.9em;
    @media (min-width: 980px) {
        margin-left: 2em;
        padding: 0.5em 1.25em;
        font-size: 1em;
    }
    line-height: 1.5;
    text-decoration: none;
    text-transform: uppercase;
    color: #009CB3;
    &.button {
      background-color: #fff; 
      color: #666;
      border: 1px solid #009CB3;
      border-radius: 25px;
    }
    &:hover {
      color: #333;
    }
  }
  @media(max-width: 719px) {
    display: none;
  }
`

const SideMenu =  styled.div`
    /* Individual item */
    .bm-item {
      display: inline-block;
      /* Our sidebar item styling */
      text-decoration: none;
      margin-bottom: 2rem;
      color: #fff;
      font-size: 1.75rem;
      transition: color 0.2s;
      outline: none;
      font-weight: 500;
      &:focus {
      }
    }
    .bm-menu-wrap {
      top: 0;
      transition: all 0.5s cubic-bezier(0.84, -0.03, 0, 1.13) 0s !important;
    }
    /* Change color on hover */
    .bm-item:hover {
      color: white;
    }
    /* The rest copied directly from react-burger-menu docs */
    /* Position and sizing of burger button */
    .bm-burger-button {
      position: absolute;
      width: 36px;
      height: 30px;
      right: 1.25em;
      top: 1.25em;
    }
    /* Color/shape of burger icon bars */
    .bm-burger-bars {
      background: #373a47;
      height: 4px;
    }
    /* Position and sizing of clickable cross button */
    .bm-cross-button {
      height: 24px !important;
      width: 24px !important;
      top: 25px !important;
      right: 25px !important;
    }
    /* Color/shape of close button cross */
    .bm-cross {
    }
    /* General sidebar styles */
    .bm-menu {
      background: #009CB3;
      font-size: 1.5em;
      text-transform: uppercase;
    }
    /* Morph shape necessary with bubble or elastic */
    .bm-morph-shape {
      fill: #373a47;
    }
    /* Wrapper for item list */
    .bm-item-list {
      display: flex;
      flex-direction: column;
      justify-content: space-evenly;
      line-height: 1.5;
      text-align: center;
      color: #fff;
    }
    /* Styling of overlay */
    .bm-overlay {
      top: 0;
      background: rgba(0, 0, 0, 0.3);
    }
    @media(min-width: 720px) {
      display: none;
    }
`